# Claim-Analysis

In this project the different machine learning methods has been used to fit the model on the healht insurance data. The goal is the estimate the cost of claim that
an insurance company may have to pay to the policyholder in case of the event insured. 

We have limited information on the predictors, in practice there are more and different types of predictors,  final set of predictors includesvariables Age, 
Weekly Wages, Hours Worked Per Week, Days Worked Per Week, Marital Status, Dependent Children, Dependent Others, Part-time/Full-time job, and Initial Claim Cost.

# Methods Used are as follows
- Linear Regression
- Polynomial Regression
- Generalized Additive Models
- Bagging Regression
- Random Forest Regression
- Gradient Boosting Regression
- Artificial Neural Network
